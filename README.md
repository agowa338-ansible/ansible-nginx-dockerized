Nginx Docker
=========

Spawns a nginx docker container

Role Variables
--------------

| Name                     | Default Value      | Usage                                                 |
|--------------------------|--------------------|-------------------------------------------------------|
| nginx_project_directory  | '/srv/nginx'       | Directory on the host where the GitLab data is stored |
| nginx_expose_http        | true               | If true http port will be exposed                     |
| nginx_expose_https       | true               | If true https port will be exposed                    |
| nginx_http_port          | 80                 | External http port                                    |
| nginx_http_host_ip       | 0.0.0.0            | Host interface to bind the http port to               |

License
-------

MIT
