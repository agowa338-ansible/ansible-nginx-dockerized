#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ROOT=$DIR/..
pushd $DIR
ansible-playbook -i $DIR/inventory.ini -vvvv --check --step $DIR/play-test.yml
popd
